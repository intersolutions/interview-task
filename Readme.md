The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC 2119](http://tools.ietf.org/html/rfc2119).

# 1. Overwier
This document describe interview task given to applicant for verify his/her basic programming skills.
If you have any questions please contact to lukasz@intersolutions.ie

# 2. Specification
Design and implement simple MVC application that will contains three controllers, one with information about you. Second controller with request contact form that will send a fake request to **apply@intersolutions.ie** and store this information in SQLite database (or other file database supported by the PHP). The last controller should list all information about stored contact requests.

Controllers, models and views should be separated through files. Provide a valid loader for them.

All required dependencies are described in composer.json (see Dependency installation section if you do not now what is a Composer). **YOU SHOULD NOT USE ANY OTHER VENDOR SOFTWARE IN BACKEND** we want to check how you think, not how you mastering one or another framework.


## 2.1 Bootstrap file
Application must be runnable from php builid in server and use PHP5.6 as minimal language version - apache or ngnix are not allowed here. Bootstrap file must process request, call properly controllers and feed response with rendered view.

## 2.2 Template engine
For view please use one from this template engine:
 - Jade, 
 - Smarty, 
 - Mustaches
or if you not familiar with them, use plain HTML mixed with PHP.

## 2.3 Views details
## 2.3.1 "About" view
If you do not now how you can describe yourself, please use standard lorem ipsum text using their [API call](http://www.lipsum.com/feed/json). You can prepare request from PHP controller or from view using Javascript AJAX request, feel free.

## 2.3.2 "Contact request" view
Contact request page must contains field:
 - mail
 - user name
 - message
 - file input

Request contact form must be validated in frotend (using javascript) and backend (in controller).
File shall be attached to mail. Send request contact mail to **apply@intersolutions.ie**. 
**To implement mail sending method you should use PHPMailer.**

## 2.4 Frontend
Feel free to do anything with site design and technologies. If you want to use AngularJs, EmberJs, REACT, Backbone use it. You prefer jQuery/Zepto that plain javascript, don't be shy. You like inlucde Validatejs for validation, do it. If you don't familiar with deasign use Bootstrap or Foundation, or provide your custom stylesheet. 
Remember that layout must be in scheme

```
     SITE NAME
-------------------
       NAV
-------------------
     CONTENT
-------------------
      FOOTER  
```

# 3. Dependency installation
If you now what is a Vagrant - please tell us and do not read this section anymore, you probably know the Composer too.

## 3.1 More automated option - with Composer
**First of all, read about composer.**
To easily install dependencies and automatically create autoloader for them proceess [this steps](https://getcomposer.org/doc/00-intro.md)

To install dependencies run in your terminal/console
```
composer install 
```
then to your main file require autoload.php from generated directory (vendors).

## 3.2 Other options
If you do not want to learning composer (is not good, we use it). You can create vendor directory and include all dependencies manually or build your own autoloader using [PSR-4 spec](http://www.php-fig.org/psr/psr-4/)

# 4. TIME FOR TASK
You have limited time for complete this task to **THREE WEEKS**. This task can be completed in one day but we know that you can have some time to think .

# 5. Codding style
Show yourself.